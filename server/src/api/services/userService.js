import userRepository from '../../data/repositories/userRepository';

export const getUserById = async userId => {
  const { id, username, email, imageId, image, status } = await userRepository.getUserById(userId);
  return { id, username, email, imageId, image, status };
};

export const updateUserStatus = async user => {
  const {
    id,
    username,
    email,
    imageId,
    image,
    status
  } = await userRepository.updateById(user.id, { status: user.status });
  return { id, username, email, imageId, image, status };
};
