/* eslint-disable array-callback-return */
import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';
import userRepository from '../../data/repositories/userRepository';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

export const updatePost = async post => postRepository.updateById(post.id, { body: post.body });

export const setReaction = async (userId, { postId, isLike = true }) => {
  // define the callback for future use as a promise
  const updateOrDelete = react => (react.isLike === isLike
    ? postReactionRepository.deleteById(react.id)
    : postReactionRepository.updateById(react.id, { isLike }));

  const reaction = await postReactionRepository.getPostReaction(userId, postId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({ userId, postId, isLike });

  // the result is an integer when an entity is deleted
  return Number.isInteger(result) ? {} : postReactionRepository.getPostReaction(userId, postId);
};

export const getUserReactions = async userId => {
  const reactions = await postReactionRepository.getUserReactions(userId);
  return reactions;
};

export const getAllReactions = async () => {
  const allReactions = await postReactionRepository.getAll();
  return allReactions;
};

export const getLikedUsers = async postId => {
  const reactions = await postReactionRepository.getAll();
  const likedUsers = reactions.filter(reaction => reaction.postId === postId && reaction.isLike);
  const users = [];

  if (likedUsers) {
    const allUsers = await userRepository.getAll();
    const newUsers = [];

    allUsers.map(user => {
      likedUsers.map(liked => {
        if (user.id === liked.userId) {
          newUsers.push({ username: user.username, image: user.imageId });
        }
      });
    });
    return newUsers;
  }

  return users;
};

export const softDeletePost = postId => postRepository.softDeletePost(postId);
