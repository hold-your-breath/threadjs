import callWebApi from 'src/helpers/webApiHelper';

export const updateUserStatus = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/user',
    type: 'PUT',
    request
  });
  return response.json();
};
