import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'semantic-ui-react';

const USERNAME_MAX_LENGTH = 5;

const LikedUsers = ({ likedUser }) => (
  <div style={{ paddingLeft: '4px' }}>
    <div style={{ textAlign: 'center' }}>
      {
        likedUser.username.length > USERNAME_MAX_LENGTH
          ? `${likedUser.username.substring(0, USERNAME_MAX_LENGTH)}..` : likedUser.username
      }
    </div>
    <Icon name="user circle" size="large" />
  </div>
);

LikedUsers.propTypes = {
  likedUser: PropTypes.shape(PropTypes.object).isRequired
};

export default LikedUsers;
