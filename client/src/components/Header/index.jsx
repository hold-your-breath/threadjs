import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { Header as HeaderUI, Image, Grid, Icon, Button } from 'semantic-ui-react';

import styles from './styles.module.scss';

const Header = ({ user, logout, updateUserStatus }) => {
  const [status, setStatus] = useState(user.status);
  const [editMode, setEditMode] = useState(false);

  const handleEditStatus = () => {
    updateUserStatus({ id: user.id, status });
    setEditMode(false);
  };
  return (
    <div className={styles.headerWrp}>
      <Grid centered container columns="2">
        <Grid.Column>
          {user && (
            <NavLink exact to="/">
              <HeaderUI style={{ display: 'flex' }}>
                <Image circular src={getUserImgLink(user.image)} />
                {' '}
                <div className={styles.userInfo}>
                  <div className={styles.username}>
                    {user.username}
                  </div>
                  { editMode
                    ? (
                      <div className={styles.status}>
                        <input type="text" value={status} onChange={e => setStatus(e.target.value)} />
                        <button type="button" onClick={handleEditStatus}>Update</button>
                        <Icon name="pencil" size="small" onClick={() => setEditMode(!editMode)} />
                      </div>
                    )
                    : (
                      <div className={styles.status}>
                        {user.status}
                        {' '}
                        <Icon name="pencil" size="small" onClick={() => setEditMode(!editMode)} />
                      </div>
                    )}
                </div>
              </HeaderUI>
            </NavLink>
          )}
        </Grid.Column>
        <Grid.Column textAlign="right">
          <NavLink exact activeClassName="active" to="/profile" className={styles.menuBtn}>
            <Icon name="user circle" size="large" />
          </NavLink>
          <Button basic icon type="button" className={`${styles.menuBtn} ${styles.logoutBtn}`} onClick={logout}>
            <Icon name="log out" size="large" />
          </Button>
        </Grid.Column>
      </Grid>
    </div>
  );
};

Header.propTypes = {
  logout: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  updateUserStatus: PropTypes.func.isRequired
};

export default Header;
