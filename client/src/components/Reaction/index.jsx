import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Label, Icon } from 'semantic-ui-react';

const Reaction = ({ className, reactionsCount, onClick, type }) => {
  const [iconType, setIconType] = useState('thumbs up');

  useEffect(() => {
    switch (type) {
      case 'dislike': setIconType('thumbs down'); break;
      case 'comment': setIconType('comment'); break;
      case 'share': setIconType('share alternate'); break;
      case 'reply': setIconType('reply'); break;
      default: setIconType('thumbs up'); break;
    }
  }, [type]);

  return (
    <Label basic size="small" as="a" className={className} style={{ border: 0 }} onClick={onClick}>
      <Icon name={iconType} />
      {reactionsCount}
    </Label>
  );
};

Reaction.propTypes = {
  className: PropTypes.string,
  reactionsCount: PropTypes.oneOfType([PropTypes.string || PropTypes.number]).isRequired,
  onClick: PropTypes.func.isRequired,
  type: PropTypes.string
};

Reaction.defaultProps = {
  className: '',
  type: 'thumb up'
};

export default Reaction;
