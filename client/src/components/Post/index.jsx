import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon, Popup } from 'semantic-ui-react';
import moment from 'moment';
import styles from './styles.module.scss';
import LikedUsers from '../LikedUsers';
import Reaction from '../Reaction';

const Post = ({
  post, likePost, dislikePost, showLikedUsers, toggleExpandedPost, toggleExpandedUpdatedPost, sharePost, softDeletePost
}) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    likedUsers,
    dislikeCount,
    commentCount,
    createdAt
  } = post;

  useEffect(() => {
    showLikedUsers(id);
  }, [id, showLikedUsers, likeCount]);

  const date = moment(createdAt).fromNow();
  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
          <div className={styles.actionBtnGroup}>
            { toggleExpandedUpdatedPost
              ? (
                <Label
                  basic
                  size="small"
                  as="a"
                  className={styles.editBtn}
                  onClick={() => toggleExpandedUpdatedPost(id)}
                >
                  <Icon name="edit outline" />
                </Label>
              )
              : null }
            { softDeletePost
              ? (
                <Label
                  basic
                  size="small"
                  as="a"
                  className={styles.closeBtn}
                  onClick={() => softDeletePost(id)}
                >
                  <Icon name="close" />
                </Label>
              )
              : null}
          </div>
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra style={{ display: 'flex' }}>
        <Popup
          content={likedUsers && likedUsers.length
            ? (
              <div style={{ display: 'flex' }}>
                {likedUsers.map(liked => <LikedUsers key={liked.username} likedUser={liked} />)}
              </div>
            )
            : 'Nobody liked this'}
          trigger={(
            <div>
              <Reaction
                className={styles.toolbarBtn}
                reactionsCount={likeCount}
                type="like"
                onClick={() => likePost(id)}
              />
            </div>
          )}
        />
        <div>
          <Reaction
            className={styles.toolbarBtn}
            reactionsCount={dislikeCount}
            type="dislike"
            onClick={() => dislikePost(id)}
          />
        </div>
        <div>
          <Reaction
            className={styles.toolbarBtn}
            reactionsCount={commentCount}
            type="comment"
            onClick={() => toggleExpandedPost(id)}
          />
        </div>
        <div>
          <Reaction
            className={styles.toolbarBtn}
            reactionsCount={' '}
            type="share"
            onClick={() => sharePost(id)}
          />
        </div>
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  showLikedUsers: PropTypes.func.isRequired,
  toggleExpandedUpdatedPost: PropTypes.func,
  softDeletePost: PropTypes.func
};

Post.defaultProps = {
  toggleExpandedUpdatedPost: undefined,
  softDeletePost: undefined
};

export default Post;
