import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';
import Reaction from '../Reaction';

const Comment = ({ comment, softDeleteComment }) => {
  const { id, user, body, createdAt } = comment;
  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          <div>{user.status}</div>
          <div>{moment(createdAt).fromNow()}</div>
          <div>
            { softDeleteComment
              ? (
                <Label
                  basic
                  size="small"
                  as="a"
                  className={styles.deleteCommentBtn}
                  onClick={() => softDeleteComment(id)}
                >
                  <Icon name="close" />
                </Label>
              )
              : null}
          </div>
        </CommentUI.Metadata>
        <CommentUI.Text>
          {body}
        </CommentUI.Text>
        <CommentUI.Actions>
          <CommentUI.Action>
            <Reaction
              reactionsCount={0}
              onClick={() => {}}
              type="like"
            />
          </CommentUI.Action>
          <CommentUI.Action>
            <Reaction
              reactionsCount={0}
              onClick={() => {}}
              type="dislike"
            />
          </CommentUI.Action>
          <CommentUI.Action>
            <Reaction
              reactionsCount={' '}
              onClick={() => {}}
              type="reply"
            />
          </CommentUI.Action>
        </CommentUI.Actions>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  softDeleteComment: PropTypes.func
};

Comment.defaultProps = {
  softDeleteComment: undefined
};

export default Comment;
